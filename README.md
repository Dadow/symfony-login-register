# Symfony Framework 4.4.1 / Login & Register


## Installation
- $ composer create-project symfony/website-skeleton symfony-01 ^4.4.1

## FOR APACHE
- $ composer require symfony/apache-pack
Using version ^1.0 for symfony/apache-pack
```
    Do you want to execute this recipe?
    [y] Yes
    [n] No
    [a] Yes for all packages, only for the current installation session
    [p] Yes permanently, never ask again for this project
    (defaults to n): y
```

## MAKE USER TABLE
- $ php bin/console make:user
- $ php bin/console make:migration
- $ php bin/console doctrine:migrations:migrate

- $ php bin/console make:auth

```
1
LoginFormAuthenticator
SecurityController


 created: src/Security/LoginFormAuthenticator.php
 updated: config/packages/security.yaml
 created: src/Controller/SecurityController.php
 created: templates/security/login.html.twig
```

## CREATING THE CONTROLLERS 
- $ php bin/console make:controller Home
```
 created: src/Controller/HomeController.php
 created: templates/home/index.html.twig
```

- $ php bin/console make:controller Register
```
 created: src/Controller/RegisterController.php
 created: templates/register/index.html.twig
```
